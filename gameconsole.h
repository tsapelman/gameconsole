#ifndef __GAME_CONSOLE__H
#define __GAME_CONSOLE__H

#include <stdbool.h>
#include <wchar.h>

// Коды управляющих клавиш
typedef enum game_console_keys_t
{
    GC_KEY_NONE          = 256 + 255,
    GC_KEY_UNKNOWN       = 256 + 254,

    GC_KEY_SPACE         =  32,
    GC_KEY_EXCLAM        =  33,
    GC_KEY_QUOTE         =  34,
    GC_KEY_SHARP         =  35,
    GC_KEY_DOLLAR        =  36,
    GC_KEY_PERCENT       =  37,
    GC_KEY_AMPERSAND     =  38,
    GC_KEY_APOSTROPHE    =  39,
    GC_KEY_PAREN_LEFT    =  40,
    GC_KEY_PAREN_RIGHT   =  41,
    GC_KEY_ASTERISK      =  42,
    GC_KEY_PLUS          =  43,
    GC_KEY_COMMA         =  44,
    GC_KEY_MINUS         =  45,
    GC_KEY_PERIOD        =  46,
    GC_KEY_SLASH         =  47,

    GC_KEY_DIGIT_BASE    =  48,
    GC_KEY_0             =  GC_KEY_DIGIT_BASE,
    GC_KEY_1             =  49,
    GC_KEY_2             =  50,
    GC_KEY_3             =  51,
    GC_KEY_4             =  52,
    GC_KEY_5             =  53,
    GC_KEY_6             =  54,
    GC_KEY_7             =  55,
    GC_KEY_8             =  56,
    GC_KEY_9             =  57,

    GC_KEY_COLON         =  58,
    GC_KEY_SEMICOLON     =  59,
    GC_KEY_LESS          =  60,
    GC_KEY_EQUAL         =  61,
    GC_KEY_GREATER       =  62,
    GC_KEY_QUESTION      =  63,
    GC_KEY_AT            =  64,

    GC_KEY_LETTER_BASE   =  65,
    GC_KEY_A             =  GC_KEY_LETTER_BASE,
    GC_KEY_B             =  66,
    GC_KEY_C             =  67,
    GC_KEY_D             =  68,
    GC_KEY_E             =  69,
    GC_KEY_F             =  70,
    GC_KEY_G             =  71,
    GC_KEY_H             =  72,
    GC_KEY_I             =  73,
    GC_KEY_J             =  74,
    GC_KEY_K             =  75,
    GC_KEY_L             =  76,
    GC_KEY_M             =  77,
    GC_KEY_N             =  78,
    GC_KEY_O             =  79,
    GC_KEY_P             =  80,
    GC_KEY_Q             =  81,
    GC_KEY_R             =  82,
    GC_KEY_S             =  83,
    GC_KEY_T             =  84,
    GC_KEY_U             =  85,
    GC_KEY_V             =  86,
    GC_KEY_W             =  87,
    GC_KEY_X             =  88,
    GC_KEY_Y             =  89,
    GC_KEY_Z             =  90,

    GC_KEY_BRACKET_LEFT  =  91,
    GC_KEY_BACKSLASH     =  92,
    GC_KEY_BRACKET_RIGHT =  93,
    GC_KEY_ASCII_CIRCUM  =  94,
    GC_KEY_UNDERSCORE    =  95,
    GC_KEY_QUOTE_LEFT    =  96,
    GC_KEY_BRACE_LEFT    = 123,
    GC_KEY_BAR           = 124,
    GC_KEY_BRACE_RIGHT   = 125,
    GC_KEY_ASCII_TILDE   = 126,

    GC_KEY_ESC           = 256 + 0,
    GC_KEY_TAB           = 256 + 1,
    GC_KEY_BACKSPACE     = 256 + 3,
    GC_KEY_ENTER         = 256 + 4,
    GC_KEY_DIGIT_ENTER   = 256 + 5,
    GC_KEY_INSERT        = 256 + 6,
    GC_KEY_DELETE        = 256 + 7,
    GC_KEY_PAUSE         = 256 + 8,

    GC_KEY_CENTER        = 256 + 11,
    GC_KEY_HOME          = 256 + 16,
    GC_KEY_END           = 256 + 17,
    GC_KEY_LEFT          = 256 + 18,
    GC_KEY_UP            = 256 + 19,
    GC_KEY_RIGHT         = 256 + 20,
    GC_KEY_DOWN          = 256 + 21,
    GC_KEY_PAGE_UP       = 256 + 22,
    GC_KEY_PAGE_DOWN     = 256 + 23,

    GC_KEY_SHIFT         = 256 + 32,
    GC_KEY_CONTROL       = 256 + 33,
    GC_KEY_ALT           = 256 + 35,
    GC_KEY_CAPS_LOCK     = 256 + 36,
    GC_KEY_NUM_LOCK      = 256 + 37,
    GC_KEY_SCROLL_LOCK   = 256 + 38,

    GC_KEY_FUNC_BASE     = 256 + 48,
    GC_KEY_F1            = GC_KEY_FUNC_BASE,
    GC_KEY_F2            = 256 + 49,
    GC_KEY_F3            = 256 + 50,
    GC_KEY_F4            = 256 + 51,
    GC_KEY_F5            = 256 + 52,
    GC_KEY_F6            = 256 + 53,
    GC_KEY_F7            = 256 + 54,
    GC_KEY_F8            = 256 + 55,
    GC_KEY_F9            = 256 + 56,
    GC_KEY_F10           = 256 + 57,
    GC_KEY_F11           = 256 + 58,
    GC_KEY_F12           = 256 + 59,

    GC_KEY_SUPER_LEFT    = 256 + 83,
    GC_KEY_SUPER_RIGHT   = 256 + 84,
    GC_KEY_MENU          = 256 + 85,

} GCKeys;

// Допустимые значения для цвета фона и цвета символов
typedef enum game_console_colors_t
{
    GC_COLOR_BLACK_DARK = 0,
    GC_COLOR_RED_DARK,
    GC_COLOR_GREEN_DARK,
    GC_COLOR_YELLOW_DARK,
    GC_COLOR_BLUE_DARK,
    GC_COLOR_MAGENTA_DARK,
    GC_COLOR_CYAN_DARK,
    GC_COLOR_WHITE_DARK,
    GC_COLOR_BLACK_BRIGHT = 16,
    GC_COLOR_RED_BRIGHT,
    GC_COLOR_GREEN_BRIGHT,
    GC_COLOR_YELLOW_BRIGHT,
    GC_COLOR_BLUE_BRIGHT,
    GC_COLOR_MAGENTA_BRIGHT,
    GC_COLOR_CYAN_BRIGHT,
    GC_COLOR_WHITE_BRIGHT
} GCColors;

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus


/* Инициализирует библиотеку */
bool gcInit(const wchar_t *caption, int width, int height);

/* Заканчивает работу с библиотекой */
void gcEnd(void);

///* Изменяет ширину и высоту окна в символах */
//bool gcResizeTo(int width, int height);

/* Задает цвет фона */
void gcSetBackgroundColor(GCColors color);

/* Задает цвет символов */
void gcSetTextColor(GCColors color);

/* Очищает окно полностью, курсор перемещает в позицию (0, 0) */
void gcClearScreen(void);

/* Очищает окно от курсора и до конца окна */
void gcClearEndOfScreen(void);

/* Очищает окно от курсора до конца строки */
void gcClearEndOfLine(void);

/* Устанавливает курсор в позицию (X, Y). *
 * X и Y отсчитываются от 0               */
void gcGotoXY(int x, int y);

/* Возвращает код нажатой клавиши или GC_NONE, *
 * если клавиша не была нажата                 */
GCKeys gcReadKey(void);

/* Возвращает код нажатой клавиши или GC_NONE,  *
 * если клавиша не была нажата, но не считывает *
 * клавишу из буфера ввода                      */
GCKeys gcPeekKey(void);

/* Очищает буфер клавиатуры ("забывает" нажатые ранее клавиши) */
void gcClearKeys(void);

/* Возвращает ширину окна (в символах) */
int gcGetWidth(void);

/* Возвращает высоту окна (в символах) */
int gcGetHeight(void);

/* Приостанавливает выполнение программы на ms миллисекунд */
void gcPause(unsigned int ms);


/*** Функции печати отдельного символа, строки или      ***
 *** форматированной строки. Общая особенность всех     ***
 *** этих функций - они работают до первого непечатного ***
 *** символа, например, "\n". При встрече непечатного   ***
 *** символа происходит (корректный) выход из функции.  ***
 *** Если необходимо напечатать несколько строк, нужно  ***
 *** вызвать соответствующую функцию несколько раз,     ***
 *** печатая строки по-отдельности.                     ***
 *** Для имитации символа табуляции воспользуйтесь      ***
 *** функцией gcSimulateTab(void).                      ***/

/* Печатает символ ch (в кодировке Unicode) в *
 * текущей позиции курсора.                   */
void gcPutChar(wchar_t ch);

/* Перемещает курсор в заданную позицию и печатает *
 * в ней символ ch (в кодировке Unicode)           */
void gcPutCharTo(int x, int y, wchar_t ch);

/* Печатает строку символов (в кодировке Unicode) *
 * с текущей позиции курсора.                     */
void gcPutText(const wchar_t *str);

/* Перемещает курсор в заданную позицию и печатает *
 * строку символов (в кодировке Unicode).          */
void gcPutTextTo(int x, int y, const wchar_t *str);

/* Печатает форматированную строку символов (в    *
 * кодировке Unicode) с текущей позиции курсора.  */
void gcPrintf(wchar_t* format, ...);

/* Перемещает курсор в заданную позицию и печатает *
 * форматированную строку символов (в кодировке    *
 * Unicode).                                       */
void gcPrintfTo(int x, int y, wchar_t* format, ...);

/* Устанавливает размер табуляции для gcSimulateTab(void), *
 * по умолчанию установлен размер табуляции, равный 8.     *
 * Размер табуляции должен быть меньше половины ширины     *
 * консоли, но не меньше 2.                                */
void gcSetTabSize(int tab_size);

/* Функция предназначена для имитации действия символа   *
 * табуляции - позиционировании курсора на следующей     *
 * позиции остановки табуляции по оси X. Например, если  *
 * размер табуляции установлен равным 8, а курсор        *
 * находится в левом верхнем углу консоли (x=1, y=1), то *
 * последовательные вызовы функции переместят курсор в   *
 * позиции (x=9, y=1), (x=17, y=1), (x=25, y=1)...       */
void gcSimulateTab(void);

/* Проверяет, пытался ли пользователь закрыть окно. *
 * Если пытался, возвращает true, иначе false       */
bool gcIsClosed(void);

/* Игнорировать и "забыть" о попытке пользователя закрыть окно */
void gcIgnoreCloseEvent(void);


#ifdef __cplusplus
}
#endif  // __cplusplus

#endif // __GAME_CONSOLE__H
