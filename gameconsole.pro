TARGET = gameconsole
TEMPLATE = lib
CONFIG += shared
CONFIG += c++11
QMAKE_CFLAGS+=-std=c99

QT += gui core widgets

SOURCES += \
    src/gameconsole.cpp \
    src/pause.cpp \
    src/charcell.cpp \
    src/gameconsolewidget.cpp

HEADERS += \
    gameconsole.h \
    src/pause.h \
    src/charcell.h \
    src/gameconsolewidget.h

target.path = /usr/local/lib/$(QMAKE_TARGET)

inc.path = /usr/local/include/$(QMAKE_TARGET)
inc.files = $(QMAKE_TARGET).h
inc.CONFIG = no_check_exist

INSTALLS += target inc
