#ifndef __CHARCELL__H
#define __CHARCELL__H

#include <QWidget>

class CharCell : public QWidget
{
    Q_OBJECT
public:
    explicit CharCell(QWidget *parent = 0);
    void setBackgroundColor(QColor color) { _backgroundColor = color; _backgroundImage = QImage(); }
    void setTextColor(QColor color) { _textColor = color; }
    void setBackgroundImage(QImage image) { _backgroundImage = image; }
    void setImage(QImage image)
    {
        if(!image.isNull())
            { _image = image; _text.clear(); }
    }
    void setText(QString text)
    {
        if(!text.isEmpty())
            { _text = text; _image = QImage(); }
    }

protected:
    void paintEvent(QPaintEvent *);

private:
    QColor _backgroundColor = Qt::lightGray;
    QColor _textColor = Qt::black;
    QImage _backgroundImage;
    QImage _image;
    QString _text = " ";
};

#endif // __CHARCELL__H
