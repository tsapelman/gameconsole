#include "charcell.h"
#include <QPainter>

CharCell::CharCell(QWidget *parent /*= 0*/)
    : QWidget(parent)
{
}

void CharCell::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setPen(Qt::NoPen);
    painter.setBrush(QBrush(_backgroundColor, Qt::SolidPattern));

    if(!_backgroundImage.isNull())
        painter.drawImage(0, 0, _backgroundImage);
    else
        painter.drawRect(rect());

    painter.setPen(_textColor);
    if(!_image.isNull())
        painter.drawImage(0, 0, _image);
    else
    {
        QFont font("Helvetica [Cronyx]", height() / 2, QFont::Normal);
        painter.setFont(font);
        QFontMetrics fontMetrics(font);
        int y = height() - (height() - fontMetrics.lineSpacing()) * 3 / 4;
        int x = (width() - fontMetrics.width(_text)) / 2;
        painter.drawText(x, y, /*Qt::AlignCenter,*/ _text);
    }
}
