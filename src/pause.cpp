#include "pause.h"

Pause::Pause(int ms, PauseTimerHandler handler)
    : QObject()
{
    _handler = handler;
    _id = startTimer(ms);
    if(!_id)
        handler();
}

/*virtual*/ void Pause::timerEvent(QTimerEvent *)
{
    killTimer(_id);
    _id = 0;
    _handler();
}
