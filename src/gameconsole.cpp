#include <gameconsole.h>
#include "pause.h"
#include "charcell.h"
#include "gameconsolewidget.h"
#include <QtWidgets>
#include <stdarg.h>

enum LocalConsts
{
    INTENSITY_BIT = GC_COLOR_BLACK_BRIGHT,
    MIN_X_SCALE = 8,
    MIN_Y_SCALE = 8,
    MIN_TAB_SIZE = 2,
    DEFAULT_TAB_SIZE = 8,
    WCHAR_BUFFER_SIZE = 1024
};

typedef struct GameConsoleHandle
{
    GameConsoleWidget *console;
    QSize screenSize;
    QSize consoleSize;
    QPoint consoleScale;
    QPoint cursor;
    QColor backgroundColor;
    QColor textColor;
    Pause *pause;
    int tabSize;
} GCHandle;

static GCHandle handle;
static bool initialized = false;

static wchar_t wchar_buffer[WCHAR_BUFFER_SIZE];
static QHash<GCColors, QColor> color_map;
static const int keymap[] =
{
    GC_KEY_SPACE, GC_KEY_EXCLAM, GC_KEY_QUOTE, GC_KEY_SHARP, GC_KEY_DOLLAR,
    GC_KEY_PERCENT, GC_KEY_AMPERSAND, GC_KEY_APOSTROPHE, GC_KEY_PAREN_LEFT, GC_KEY_PAREN_RIGHT,
    GC_KEY_ASTERISK, GC_KEY_PLUS, GC_KEY_COMMA, GC_KEY_MINUS, GC_KEY_PERIOD, GC_KEY_SLASH,

    GC_KEY_0, GC_KEY_1, GC_KEY_2, GC_KEY_3, GC_KEY_4, GC_KEY_5, GC_KEY_6, GC_KEY_7, GC_KEY_8, GC_KEY_9,

    GC_KEY_COLON, GC_KEY_SEMICOLON, GC_KEY_LESS, GC_KEY_EQUAL, GC_KEY_GREATER, GC_KEY_QUESTION, GC_KEY_AT,

    GC_KEY_A, GC_KEY_B, GC_KEY_C, GC_KEY_D, GC_KEY_E, GC_KEY_F, GC_KEY_G, GC_KEY_H, GC_KEY_I, GC_KEY_J,
    GC_KEY_K, GC_KEY_L, GC_KEY_M, GC_KEY_N, GC_KEY_O, GC_KEY_P, GC_KEY_Q, GC_KEY_R, GC_KEY_S, GC_KEY_T,
    GC_KEY_U, GC_KEY_V, GC_KEY_W, GC_KEY_X, GC_KEY_Y, GC_KEY_Z,

    GC_KEY_BRACKET_LEFT, GC_KEY_BACKSLASH, GC_KEY_BRACKET_RIGHT, GC_KEY_ASCII_CIRCUM, GC_KEY_UNDERSCORE,
    GC_KEY_QUOTE_LEFT, GC_KEY_BRACE_LEFT, GC_KEY_BAR, GC_KEY_BRACE_RIGHT, GC_KEY_ASCII_TILDE,

    GC_KEY_ESC, GC_KEY_TAB, GC_KEY_BACKSPACE, GC_KEY_ENTER, GC_KEY_DIGIT_ENTER,
    GC_KEY_INSERT, GC_KEY_DELETE, GC_KEY_PAUSE, GC_KEY_CENTER, GC_KEY_HOME, GC_KEY_END,
    GC_KEY_LEFT, GC_KEY_UP, GC_KEY_RIGHT, GC_KEY_DOWN, GC_KEY_PAGE_UP, GC_KEY_PAGE_DOWN,

    GC_KEY_SHIFT, GC_KEY_CONTROL, GC_KEY_ALT, GC_KEY_CAPS_LOCK, GC_KEY_NUM_LOCK, GC_KEY_SCROLL_LOCK,

    GC_KEY_F1, GC_KEY_F2, GC_KEY_F3, GC_KEY_F4, GC_KEY_F5, GC_KEY_F6, GC_KEY_F7, GC_KEY_F8,
    GC_KEY_F9, GC_KEY_F10, GC_KEY_F11, GC_KEY_F12,

    GC_KEY_SUPER_LEFT, GC_KEY_SUPER_RIGHT, GC_KEY_MENU
};

inline void setCharToCell(int x, int y, wchar_t ch);
inline void setCharToCurrentCell(wchar_t ch);
static void initColorMap();
static QColor toQColor(GCColors gcColor);
static void pauseTimerHandler();
static int fromQtKey(int key);

bool gcInit(const wchar_t *caption, int width, int height)
{
    Q_ASSERT(!initialized);
    Q_ASSERT(caption);
    QString caption_str = QString::fromWCharArray(caption);

    static char app_name[] = "gameconsole-app";
    char *argv[2] = { app_name, NULL };
    int argc = 1;
    initColorMap();
    new QApplication(argc, argv);
    qApp->setApplicationDisplayName(caption_str);
    handle.screenSize = QApplication::desktop()->screenGeometry().size();
    handle.consoleSize = QSize(width, height);
    handle.consoleScale = QPoint(
                (handle.screenSize.width() - handle.screenSize.width() / 8) / width,
                (handle.screenSize.height() - handle.screenSize.height() / 8) / height);
    if(width < 1 || height < 1
            || handle.consoleScale.x() < MIN_X_SCALE || handle.consoleScale.y() < MIN_Y_SCALE)
        return false;

    handle.console = new GameConsoleWidget();
    handle.backgroundColor = toQColor(GC_COLOR_BLACK_DARK);
    handle.textColor = toQColor(GC_COLOR_GREEN_DARK);
    QGridLayout *layout = new QGridLayout();
    layout->setSpacing(0);
    handle.pause = nullptr;

    for(int y = 0; y < height; ++y)
        for(int x = 0; x < width; ++x)
        {
            CharCell *item = new CharCell();
            item->setMaximumSize(handle.consoleScale.x(), handle.consoleScale.y());
            item->setMinimumSize(handle.consoleScale.x(), handle.consoleScale.y());
            layout->addWidget(item, y, x);
        }
    handle.console->setLayout(layout);
    handle.tabSize = DEFAULT_TAB_SIZE;
    initialized = true;
    gcClearScreen();
    handle.console->resize(
                handle.consoleSize.width() * handle.consoleScale.x(),
                handle.consoleSize.height() * handle.consoleScale.y());
    handle.console->setMaximumSize(
                handle.consoleSize.width() * handle.consoleScale.x(),
                handle.consoleSize.height() * handle.consoleScale.y());
    handle.console->setMinimumSize(
                handle.consoleSize.width() * handle.consoleScale.x(),
                handle.consoleSize.height() * handle.consoleScale.y());
    handle.console->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    handle.console->move(
                (handle.screenSize.width() - handle.console->size().width()) / 3,
                (handle.screenSize.height() - handle.console->size().height()) / 3);
    handle.console->show();
    gcPause(20);
    return true;
}

void gcEnd(void)
{
    Q_ASSERT(initialized);
    handle.console->stop();
    qApp->closeAllWindows();
    qApp->quit();
    initialized = false;
}

void gcSetBackgroundColor(GCColors color)
{
    Q_ASSERT(initialized);
    handle.backgroundColor = toQColor(color);
}

void gcSetTextColor(GCColors color)
{
    Q_ASSERT(initialized);
    handle.textColor = toQColor(color);
}

void gcClearScreen(void)
{
    Q_ASSERT(initialized);
    for(int y = 0; y < handle.consoleSize.height(); ++y)
        for(int x = 0; x < handle.consoleSize.width(); ++x)
            setCharToCell(x, y, L' ');
    handle.cursor = QPoint(0, 0);
}

void gcClearEndOfScreen(void)
{
    Q_ASSERT(initialized);
    gcClearEndOfLine();
    for(int y = handle.cursor.y()+1; y < handle.consoleSize.height(); ++y)
        for(int x = 0; x < handle.consoleSize.width(); ++x)
            setCharToCell(x, y, L' ');
}

void gcClearEndOfLine(void)
{
    Q_ASSERT(initialized);
    for(int x = handle.cursor.x(); x < handle.consoleSize.width(); ++x)
        setCharToCell(x, handle.cursor.y(), L' ');
}

void gcGotoXY(int x, int y)
{
    Q_ASSERT(initialized);
    if(x < 0) x = 0;
    if(x >= handle.consoleSize.width())
        x = handle.consoleSize.width() - 1;
    if(y < 0) y = 0;
    if(y >= handle.consoleSize.height())
        y = handle.consoleSize.height() - 1;
    handle.cursor = QPoint(x, y);
}

GCKeys gcPeekKey(void)
{
    Q_ASSERT(initialized);
    return handle.console->isKeyPressed()
            ? GCKeys(fromQtKey(handle.console->peekKey()))
            : GC_KEY_NONE;
}

GCKeys gcReadKey(void)
{
    Q_ASSERT(initialized);
    return handle.console->isKeyPressed()
            ? GCKeys(fromQtKey(handle.console->readKey()))
            : GC_KEY_NONE;
}

void gcClearKeys(void)
{
    Q_ASSERT(initialized);
    handle.console->clearKeys();
}

int gcGetWidth(void)
{
    Q_ASSERT(initialized);
    return handle.consoleSize.width();
}

int gcGetHeight(void)
{
    Q_ASSERT(initialized);
    return handle.consoleSize.height();
}

void gcPause(unsigned int ms)
{
    Q_ASSERT(initialized);
    handle.pause = new Pause(ms, pauseTimerHandler);
    qApp->exec();
    handle.pause->deleteLater();
    handle.pause = nullptr;
}

bool gcIsClosed(void)
{
    Q_ASSERT(initialized);
    return handle.console->isToClose();
}

void gcIgnoreCloseEvent(void)
{
    Q_ASSERT(initialized);
    handle.console->ignoreCloseEvent();
}

void gcPutChar(wchar_t ch)
{
    Q_ASSERT(initialized);
    if(iswprint(ch))
        setCharToCurrentCell(ch);
}

void gcPutCharTo(int x, int y, wchar_t ch)
{
    Q_ASSERT(initialized);
    gcGotoXY(x, y);
    gcPutChar(ch);
}

void gcPutText(const wchar_t *str)
{
    Q_ASSERT(initialized);
    const wchar_t *p = str;
    while(iswprint(*p))
        setCharToCurrentCell(*p++);
}

void gcPutTextTo(int x, int y, const wchar_t *str)
{
    Q_ASSERT(initialized);
    gcGotoXY(x, y);
    gcPutText(str);
}

void gcPrintf(wchar_t* format, ...)
{
    Q_ASSERT(initialized);
    va_list argList;
    va_start(argList, format);
    vswprintf(wchar_buffer, WCHAR_BUFFER_SIZE, format, argList);
    va_end(argList);
    gcPutText(wchar_buffer);
}

void gcPrintfTo(int x, int y, wchar_t* format, ...)
{
    Q_ASSERT(initialized);
    gcGotoXY(x, y);
    va_list argList;
    va_start(argList, format);
    vswprintf(wchar_buffer, WCHAR_BUFFER_SIZE, format, argList);
    va_end(argList);
    gcPutText(wchar_buffer);
}

void gcSetTabSize(int tab_size)
{
    Q_ASSERT(initialized);
    if(tab_size >= handle.consoleSize.width())
        tab_size = handle.consoleSize.width() - 1;
    if(tab_size < MIN_TAB_SIZE)
        tab_size = MIN_TAB_SIZE;
    handle.tabSize = tab_size;
}

void gcSimulateTab(void)
{
    Q_ASSERT(initialized);
    int x = handle.cursor.x();
    const int y = handle.cursor.y();
    const int w = handle.consoleSize.width();
    const int h = handle.consoleSize.height();
    if(x > 0 && x < w && y > 0 && y <= h)
    {
        const int tab = handle.tabSize;
        x = ((x-1) / tab + 1) * tab + 1;
        if(x > w)
            x = w;
        gcGotoXY(x, y);
    }
}

static void pauseTimerHandler()
{
    qApp->exit();
}

inline void setCharToCell(int x, int y, wchar_t ch)
{
    QGridLayout *layout = (QGridLayout *)handle.console->layout();
    CharCell *item = (CharCell *)layout->itemAtPosition(y, x)->widget();
    item->setBackgroundColor(handle.backgroundColor);
    item->setTextColor(handle.textColor);
    item->setText(QString(ch));
    item->update();
}

inline void setCharToCurrentCell(wchar_t ch)
{
    QGridLayout *layout = (QGridLayout *)handle.console->layout();
    CharCell *item = (CharCell *)layout->itemAtPosition(handle.cursor.y(), handle.cursor.x())->widget();
    item->setBackgroundColor(handle.backgroundColor);
    item->setTextColor(handle.textColor);
    item->setText(QString(ch));
    item->update();
    if(++handle.cursor.rx() >= handle.consoleSize.width())
    {
        handle.cursor.setX(0);
        if(handle.cursor.y() < handle.consoleSize.height()-1)
            ++handle.cursor.rx();
    }
}

static void initColorMap()
{
    GCColors gc_map[] =
    {
        GC_COLOR_BLACK_DARK, GC_COLOR_RED_DARK, GC_COLOR_GREEN_DARK, GC_COLOR_YELLOW_DARK,
        GC_COLOR_BLUE_DARK, GC_COLOR_MAGENTA_DARK, GC_COLOR_CYAN_DARK, GC_COLOR_WHITE_DARK,
        GC_COLOR_BLACK_BRIGHT, GC_COLOR_RED_BRIGHT, GC_COLOR_GREEN_BRIGHT, GC_COLOR_YELLOW_BRIGHT,
        GC_COLOR_BLUE_BRIGHT, GC_COLOR_MAGENTA_BRIGHT, GC_COLOR_CYAN_BRIGHT, GC_COLOR_WHITE_BRIGHT
    };
    QColor qt_map[] =
    {
        Qt::black, Qt::darkRed, Qt::darkGreen, Qt::darkYellow,
        Qt::darkBlue, Qt::darkMagenta, Qt::darkCyan, Qt::gray,
        Qt::darkGray, Qt::red, Qt::green, Qt::yellow,
        Qt::blue, Qt::magenta, Qt::cyan, Qt::white
    };

    color_map.clear();
    int map_size = sizeof(gc_map) / sizeof(gc_map[0]);
    Q_ASSERT(map_size == sizeof(qt_map) / sizeof(qt_map[0]));
    for(int i = 0; i < map_size; ++i)
        color_map.insert(gc_map[i], qt_map[i]);
}

static QColor toQColor(GCColors gcColor)
{
    return color_map.value(gcColor, Qt::black);
}

static int fromQtKey(int key)
{
    int keymap_size = sizeof(keymap)/sizeof(keymap[0]);
    if(key > 0 && key <= keymap_size)
        return keymap[key-1];
    return key ? GC_KEY_UNKNOWN : GC_KEY_NONE;
}
