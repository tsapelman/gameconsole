#ifndef GAMECONSOLEWIDGET_H
#define GAMECONSOLEWIDGET_H

#include <QWidget>

class GameConsoleWidget : public QWidget
{
    Q_OBJECT
    enum Consts
    {
        KeyboardRingSize = 128
    };
    typedef QHash<int, int> Keymap;

public:
    explicit GameConsoleWidget(QWidget *parent = 0);
    bool isToClose() const { return _toClose; }
    void ignoreCloseEvent();
    void stop();
    bool isKeyPressed() const;
    int peekKey() const { return isKeyPressed() ? _keyboardRing[_readKR] : 0; }
    int readKey();
    void clearKeys() { _readKR = _writeKR; }

protected:
    static Keymap keymapInitialize();
    void storeKey(int key);
    void closeEvent(QCloseEvent *);
    void keyPressEvent(QKeyEvent *);

private:
    int _keyboardRing[KeyboardRingSize];
    int _readKR = 0;
    int _writeKR = 0;
    bool _toClose = false;
    bool _stopped = false;
    static Keymap _keymap;
};

#endif // GAMECONSOLEWIDGET_H
