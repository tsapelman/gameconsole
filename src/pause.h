#ifndef __PAUSE__H
#define __PAUSE__H

#include <QObject>

typedef void (*PauseTimerHandler)();

class Pause : public QObject
{
Q_OBJECT
public:
    Pause(int ms, PauseTimerHandler handler);
    bool isActive() const { return _id; }

private:
    int _id = 0;
    PauseTimerHandler _handler = nullptr;
    virtual void timerEvent(QTimerEvent *);
};

#endif // __PAUSE__H
