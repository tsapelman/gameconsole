#include "gameconsolewidget.h"
#include <QCloseEvent>
#include <QCoreApplication>

/*static*/ GameConsoleWidget::Keymap GameConsoleWidget::_keymap = GameConsoleWidget::keymapInitialize();

/*static*/ GameConsoleWidget::Keymap GameConsoleWidget::keymapInitialize()
{
    const int keys[] =
    {
        Qt::Key_Space, Qt::Key_Exclam, Qt::Key_QuoteDbl, Qt::Key_NumberSign, Qt::Key_Dollar,
        Qt::Key_Percent, Qt::Key_Ampersand, Qt::Key_Apostrophe, Qt::Key_ParenLeft, Qt::Key_ParenRight,
        Qt::Key_Asterisk, Qt::Key_Plus, Qt::Key_Comma, Qt::Key_Minus, Qt::Key_Period, Qt::Key_Slash,

        Qt::Key_0, Qt::Key_1, Qt::Key_2, Qt::Key_3, Qt::Key_4, Qt::Key_5, Qt::Key_6, Qt::Key_7, Qt::Key_8, Qt::Key_9,

        Qt::Key_Colon, Qt::Key_Semicolon, Qt::Key_Less, Qt::Key_Equal, Qt::Key_Greater, Qt::Key_Question, Qt::Key_At,

        Qt::Key_A, Qt::Key_B, Qt::Key_C, Qt::Key_D, Qt::Key_E, Qt::Key_F, Qt::Key_G, Qt::Key_H, Qt::Key_I, Qt::Key_J,
        Qt::Key_K, Qt::Key_L, Qt::Key_M, Qt::Key_N, Qt::Key_O, Qt::Key_P, Qt::Key_Q, Qt::Key_R, Qt::Key_S, Qt::Key_T,
        Qt::Key_U, Qt::Key_V, Qt::Key_W, Qt::Key_X, Qt::Key_Y, Qt::Key_Z,

        Qt::Key_BracketLeft, Qt::Key_Backslash, Qt::Key_BracketRight, Qt::Key_AsciiCircum, Qt::Key_Underscore,
        Qt::Key_QuoteLeft, Qt::Key_BraceLeft, Qt::Key_Bar, Qt::Key_BraceRight, Qt::Key_AsciiTilde,

        Qt::Key_Escape, Qt::Key_Tab, Qt::Key_Backspace, Qt::Key_Return, Qt::Key_Enter,
        Qt::Key_Insert, Qt::Key_Delete, Qt::Key_Pause, Qt::Key_Clear, Qt::Key_Home, Qt::Key_End,
        Qt::Key_Left, Qt::Key_Up, Qt::Key_Right, Qt::Key_Down, Qt::Key_PageUp, Qt::Key_PageDown,

        Qt::Key_Shift, Qt::Key_Control, Qt::Key_Alt, Qt::Key_CapsLock, Qt::Key_NumLock, Qt::Key_ScrollLock,

        Qt::Key_F1, Qt::Key_F2, Qt::Key_F3, Qt::Key_F4, Qt::Key_F5, Qt::Key_F6, Qt::Key_F7, Qt::Key_F8,
        Qt::Key_F9, Qt::Key_F10, Qt::Key_F11, Qt::Key_F12,

        Qt::Key_Super_L, Qt::Key_Super_R, Qt::Key_Menu
    };

    Keymap keymap;
    int collection_size = sizeof(keys)/sizeof(keys[0]);
    for(int i = 0; i < collection_size; ++i)
        keymap.insert(keys[i], i+1);

    return keymap;
}

GameConsoleWidget::GameConsoleWidget(QWidget *parent)
    : QWidget(parent)
{
}

void GameConsoleWidget::ignoreCloseEvent()
{
    if(!_stopped)
        _toClose = false;
}

void GameConsoleWidget::stop()
{
    _stopped = true;
}

bool GameConsoleWidget::isKeyPressed() const
{
    if(_readKR != _writeKR)
        return true;
    QCoreApplication::processEvents();
    return false;
}

int GameConsoleWidget::readKey()
{
    int key = 0;
    if(isKeyPressed())
    {
        key = _keyboardRing[_readKR];
        _readKR = (_readKR + 1) % KeyboardRingSize;
    }
    return key;
}

void GameConsoleWidget::storeKey(int key)
{
    if(key && (_writeKR + 1) % KeyboardRingSize != _readKR)
    {
        _keyboardRing[_writeKR] = key;
        _writeKR = (_writeKR + 1) % KeyboardRingSize;
    }
}

void GameConsoleWidget::closeEvent(QCloseEvent *event)
{
    if(_stopped)
        event->accept();
    else
    {
        event->ignore();
        _toClose = true;
    }
}

void GameConsoleWidget::keyPressEvent(QKeyEvent *event)
{
    int key = event->key();
    if(_keymap.contains(key))
    {
        storeKey(_keymap.value(key));
    }
    else
        event->ignore();
}
