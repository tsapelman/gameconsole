TARGET = moving-monkey
TEMPLATE = app

QMAKE_CFLAGS+=-std=c99

SOURCES += \
    program.c \
    main.c

HEADERS += \
    program.h

GAMECONSOLE_LIB_PATH = /usr/local/lib/gameconsole

LIBS += -lgameconsole -L$${GAMECONSOLE_LIB_PATH}
QMAKE_LFLAGS += -Wl,-rpath=$${GAMECONSOLE_LIB_PATH}
