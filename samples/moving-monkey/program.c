#include <program.h>
#include <gameconsole/gameconsole.h>
#include <stdio.h>

enum Consts
{
    WIDTH = 5,
    HEIGHT = 3,
    DURATION = 20
};

static const wchar_t *application_caption = L"moving-monkey";

void move(int *xPos, int *yPos, int *xDir, int *yDir)
{
    *xPos += *xDir;
    *yPos += *yDir;
    if(*xPos <= 0)
        *xDir = 1;
    if(*xPos > gcGetWidth() - WIDTH)
    {
        *xDir = -1;
        *xPos = gcGetWidth() - WIDTH;
    }
    if(*yPos <= 0)
        *yDir = 1;
    if(*yPos > gcGetHeight() - HEIGHT)
    {
        *yDir = -1;
        *yPos = gcGetHeight() - HEIGHT;
    }
}

void display(int xPos, int yPos, bool appearance)
{
    int line = 0;
    while(line < HEIGHT)
    {
        gcGotoXY(xPos, yPos+line);
        if(appearance)
        {
            switch(line)
            {
            case 0: gcPutText( L"/^^^\\"); break;
            case 1: gcPutText( L"|O|O|");  break;
            case 2: gcPrintf (L"\\-V-/"); break;
            }
        }
        else
            gcPutText(L"     ");
        ++line;
    }
}

void makePause(void)
{
    gcPause(DURATION);
}

bool handleKey(int *xDir, int *yDir)
{
    bool finish = false;
    GCKeys ch, last;
    gcGotoXY(0, 0);
    last = ch = GC_KEY_NONE;
    while((ch = gcReadKey()) != GC_KEY_NONE)
    {
        last = ch;
    }
    if(last != GC_KEY_NONE)
        gcClearEndOfLine();

    switch(last)
    {
    case GC_KEY_LEFT:          *xDir = -1; *yDir = 0;  break;
    case GC_KEY_HOME:          *xDir = -1; *yDir = -1; break;
    case GC_KEY_UP:            *xDir = 0;  *yDir = -1; break;
    case GC_KEY_PAGE_UP:       *xDir = 1;  *yDir = -1; break;
    case GC_KEY_RIGHT:         *xDir = 1;  *yDir = 0;  break;
    case GC_KEY_PAGE_DOWN:     *xDir = 1;  *yDir = 1;  break;
    case GC_KEY_DOWN:          *xDir = 0;  *yDir = 1;  break;
    case GC_KEY_END:           *xDir = -1; *yDir = 1;  break;
    case GC_KEY_CENTER:
    case ' ':
        *xDir = 0;  *yDir = 0;  break;
    case GC_KEY_ESC:
    case GC_KEY_Q:
        *xDir = 0;  *yDir = 0; finish = true; break;
    default:
        break;
    }
    gcClearKeys();
    return finish;
}

void myProgram(void)
{
    gcInit(application_caption, 80, 25);
    int xPos = 0;
    int yPos = 0;
    int xDir = 1;
    int yDir = 1;
    gcClearKeys();
    gcSetTextColor(GC_COLOR_WHITE_BRIGHT);
    gcSetBackgroundColor(GC_COLOR_GREEN_DARK);
    gcClearScreen();
    display(xPos, yPos, true);
    makePause();
    while(gcReadKey() == GC_KEY_NONE)
        ;

    int i = 1000;
    int tries = 3;
    bool finish = false;
    while(!finish)
    {
        if(i == 500)
        {
            gcEnd();
            gcInit(application_caption, 40, 18);
            gcSetTextColor(GC_COLOR_WHITE_BRIGHT);
            gcSetBackgroundColor(GC_COLOR_GREEN_DARK);
            gcClearScreen();
        }
        i--;
        finish = handleKey(&xDir, &yDir);
        display(xPos, yPos, false);
        move(&xPos, &yPos, &xDir, &yDir);
        display(xPos, yPos, true);
        makePause();
        if(gcIsClosed())
        {
            --tries;
            if(tries == 0)
                finish = true;
            else
                gcIgnoreCloseEvent();
        }
        finish = finish || (i == 0);
    }
    gcEnd();
}
